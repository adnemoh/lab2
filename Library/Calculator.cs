﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Library
{
    public class Calculator
    {
        private static readonly string[] baseDelimiters = new[] { ",", "\n" };
        private static Regex delimitersRegex = new Regex(@"\[[^0-9\[\]]*\]");

        public int Calculate(string input)
        {
            string[] delimiters;
            (delimiters, input) = ExtractDelimiters(input);
            delimiters = baseDelimiters.Concat(delimiters).ToArray();

            return input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Select(ParseInt).Sum();
        }

        private (string[] delimiters, string input) ExtractDelimiters(string rawInput)
        {
            if (rawInput.First() != '\\')
                return (new string[0], rawInput);

            if (rawInput[1] != '[')
                return (new[] { rawInput[1].ToString() }, rawInput.Substring(2));

            var matches = delimitersRegex.Matches(rawInput);
            var delimiters = new List<string>();
            // for(var i = 0; i < matches.Count; ++i)
            // {
            //     var match = matches[i]
            // }

//            var input = rawInput.Substring(lastDelimiter + 2);
            return (new string[0], rawInput);
        }

        private int ParseInt(string x)
        {
            var result = int.Parse(x);
            if (result < 0)
                throw new NegativeNumberException();
            return result > 1000 ? 0 : result;
        }
    }

    public class NegativeNumberException : Exception
    { }
}
