﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Library.Tests
{
    public class CalculatorTests
    {

        private Calculator calculator = new Calculator();

        // [Fact]
        // public void Calculate_empty_string_gets_zero()
        // {
        //     var result = calculator.Calculate("");

        //     Assert.Equal(0, result);
        // }

        [Theory]
        [InlineData("3", 3)]
        public void Calculate_single_int_on_input_gets_returned(string input, int expected)
        {
            var result = calculator.Calculate(input);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("2,3", 5)]
        [InlineData("2,3,1", 6)]
        public void Calculate_comma_separated_ints_return_sum(string input, int expected)
        {
            var result = calculator.Calculate(input);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("5\n3",8)]
        [InlineData("5\n3\n4",12)]
        public void Calculate_newline_separated_ints_return_sum(string input, int expected)
        {
            var result = calculator.Calculate(input);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("-5")]
        [InlineData("-4,23,3")]
        [InlineData("3,4,5\n-5")]
        public void Calculate_negative_number_throws_exception(string input)
        {
            Assert.Throws<NegativeNumberException>(() => calculator.Calculate(input));
        }

        [Theory]
        [InlineData("1001", 0)]
        [InlineData("1000,1", 1001)]
        [InlineData("1,2,3,1004", 6)]
        public void Calculate_numbers_above_thousand_are_ignored(string input, int expected)
        {
            var result = calculator.Calculate(input);
            Assert.Equal(expected, result);
        }


        // [Theory(Skip = "Doesn't work :(")]
        // [InlineData("\\#3#4#5", 12)]
        // [InlineData("\\[##]3##4##5", 12)]
        // public void Calculate_can_declare_new_delimiters(string input, int expected)
        // {
        //     var result = calculator.Calculate(input);
        //     Assert.Equal(expected, result);
        // }
    }
}
